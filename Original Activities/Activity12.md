<!-- Output copied to clipboard! -->


<p style="color: red; font-weight: bold">>>>>>  gd2md-html alert:  ERRORs: 0; WARNINGs: 2; ALERTS: 2.</p>
<ul style="color: red; font-weight: bold"><li>See top comment block for details on ERRORs and WARNINGs. <li>In the converted Markdown or HTML, search for inline alerts that start with >>>>>  gd2md-html alert:  for specific instances that need correction.</ul>

<p style="color: red; font-weight: bold">Links to alert messages:</p><a href="#gdcalert1">alert1</a>
<a href="#gdcalert2">alert2</a>

<p style="color: red; font-weight: bold">>>>>> PLEASE check and correct alert issues and delete this message and the inline alerts.<hr></p>



    _Version 2019-Fall-1.0, Revised 29 October 2019_


# 
    Activity 12: 


# 
    REST API Frontend Code


## 
    Content Learning Objectives


    _After completing this activity, students should be able to:_



*   

## 
     Process Skill Goals


    _During the activity, students should make progress toward:_

*   

## 
    Team Roles


    _Decide what role each of you will play for today. Choose a role that you have not played before, or recently. The goal should be to have all team members rotate through the roles on a regular basis to become comfortable with all the roles. If you have only three people, one should have two roles. If you have five people, two may share the same role. Record role assignments here._


<table>
  <tr>
   <td>
Manager
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>Presenter
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>Recorder
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>Technician/
<p>
Reflector
   </td>
   <td>
   </td>
  </tr>
</table>



# 


# Model 1: Set Up

Your Technician definitely needs to do this. Everybody may want to do this on their own computer to prepare for future work.


## Steps (10 min)

Things to do first to get ready:



*   Install a CORS plugin on the browser you plan to use:
    *   Firefox: [CORS Everywhere](https://addons.mozilla.org/en-US/firefox/addon/cors-everywhere/)
    *   Chrome: [Allow-Control-Allow-Origin: *](https://chrome.google.com/webstore/detail/allow-control-allow-origi/nlfbmbojpeacfghkpbjhddihlkkiljbi?utm_source=chrome-app-launcher-info-dialog)
*   Start the REST-API-Order-System backend. 
    *   Open your command line/terminal in the folder where you cloned the code and issue the command **<code>./gradlew clean bootrun</code></strong>
*   Start Insomnia.
    *   You will want to use Insomnia to add data to retrieve with your Angular app, or get data added/modified by your Angular app.
*   Install Angular CLI
    *   Using the [Angular CLI](https://cli.angular.io/) will make things much easier. It will let you easily create the framework for new apps, create skeletons for new classes and components within an existing app, and start a local development server to run your app.
    *   To install the Angular CLI, type the following command in your terminal/command prompt window <strong><code>npm install -g @angular/cli</code></strong>


# 


# Model 2: Building an Angular Project


## Steps (5 min)



*   Create a new Angular project. 
    *   You have two choices for how to do this (both using the Angular CLI):
        *   Open your command line/terminal in the folder where you want to create the project and issue the command **<code>ng new activity12</code></strong>
        *   Start WebStorm
            *   You can create a new App (ng new) from File>New>Project… and choosing Angular CLI.
                *   Name your project under location
                *   Make sure the correct version of the Node Interpreter is selected (not red)
                *   Make sure the correct version of the Angular CLI is selected
    *   In both cases, just hit return for the questions about Routing and CSS.
*   Start an Angular server running your app:
    *   From the terminal/command line or from the terminal in WebStorm run the command: <strong><code>ng serve -o</code></strong>
    *   The server will start and open your browser connected to the server.


## Questions (10 min)



1. Open the **<code>index.html</code></strong> file.
    1. What line puts “activity12” on the browser tab?
    2. What line puts the Angular icon on the browser tab?
    3. What line puts the content below into the browser window?



<p id="gdcalert1" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline drawings not supported directly from Docs. You may want to copy the inline drawing to a standalone drawing and export by reference. See <a href="https://github.com/evbacher/gd2md-html/wiki/Google-Drawings-by-reference">Google Drawings by reference</a> for details. The img URL below is a placeholder. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert2">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![drawing](https://docs.google.com/drawings/d/12345/export/png)

        1. Paste the code here.
2. Open the **<code>src/app </code></strong>folder. 
    4. How many files are there?
    5. List their names.
3. Each Angular app has one base module. 
    6. Which file defines this module? 
    7. What type of file is this?
    8. Open the file.
    9. This file contains the equivalent of the main method, in that it defines the component that starts the app. 
        2. On what line is that done? 
        3. What is the name of the component?
4. What files define the main component?
5. What is the purpose of each of the files?
6. Open the typescript file. Find the connection between this file and your answer to Question 1c. How are they connected?
    10. What is this doing?
7. Open the HTML file. What is the connection between this file and your answer to Question 1c?
8. What is happening on line 4 with <strong><code>{{ title }}</code></strong>?
9. Change the value being assigned to the title variable in the typescript file, and switch back to your browser window. What happened? What does this tell you about the <strong><code>{{ }} </code></strong>notation?


# 


# Model 3: Making REST API Calls - GET

Now we are going to make calls to the REST API backend from the Angular App.

We are going to use the HttpClientModule to do this.


## Steps and Questions (15 min)



1. Open the **<code>app.module.ts</code></strong> file.
    1. Add the <strong><code>HttpClientModule</code></strong> to the <strong><code>imports</code></strong> array. The modules must be separated by commas.
    2. If you are using WebStorm, a list appears that you can choose the module from.
    3. If you are using WebStorm, an import statment will be added for the module as well.
2. Open the <strong><code>app.component.ts</code></strong> file.
    4. Add the following constructor to the class:

        ```
        constructor(private http: HttpClient){
        }
        ```


    5. Add the import statement for the HttpClient:

        ```
        import { HttpClient } from '@angular/common/http';
        ```


3. Now we are going to add an HTTP GET request to get a Customer, given the customer number.
    6. Add an ngOnInit() method:

        ```
        ngOnInit(): void {
         this.http.get('http://localhost:8080/customers/1')
           .subscribe(data => {
             console.log(data);
           }
         );
        }
        ```


    7. When do you think this method will be run? Why?
    8. What data are we getting from the server?
    9. Where will the returned data be displayed? Why?
    10. What do you think the **<code>.subscribe()</code></strong> is there for? Do you think that the server will return the data immediately?
    11. What do you think the <strong><code>( data => { console.log(data) } )</code></strong> does? And I am not asking about the <strong><code>console.log</code></strong> part.
4. Open your browser.
    12. Make sure your CORS plugin is on - it should be green.
    13. Refresh your browser window.
    14. Do you see any output?
    15. Find the console on your browser.
        1. On Firefox, it will be under Tools > Web Developer > Web Console
        2. On Chrome, it will be under View > Developer > Javascript Console
    16. You should see a 404 error. Use Insomnia to add a Customer to your backend server.
    17. Refresh your browser window.


# 


# Model 4: Display Results in the App

To display the results of our HTTP GET, we will convert the data to a Customer object, and they display the object’s fields in the HTML of the app.


## Steps and Questions (15 min)



1. Define a Customer class.
    1. Right-click on the app folder.
    2. Choose New > Angular Schematic…
    3. Choose class.
    4. Enter Customer as the name.
    5. Open the Customer.ts file and enter:

        ```
        export class Customer {
         number: number;
         firstName: String;
         lastName: String;
        }
        ```


    6. Replace the GET request with:

        ```
        this.http.get<Customer>('http://localhost:8080/customers/1')
         .subscribe(data => {
           console.log(data.number);
           console.log(data.firstName);
           console.log(data.lastName);
         }
        );
        ```


    7. Check the browser console for the new output.
2. Display customer data in HTML.
    8. Declare a Customer variable in AppComponent (just below title).

        ```
        customer: Customer;
        ```


    9. Save the result of the GET in the customer variable:

        ```
        this.http.get<Customer>('http://localhost:8080/customers/1')
         .subscribe(data => {
           this.customer = data;
         }
        );
        ```


    10. Display in HTML by replacing the list:

        ```
        <ul>
         <li>
           Customer number: {{customer.number}}
         </li>
         <li>
           First name: {{customer.firstName}}
         </li>
         <li>
           Last name: {{customer.lastName}}
         </li>
        </ul>
        ```


    11. Check the browser window for the new output.
    12. Check the browser console for the new output. What do you see? Why do you think that happened?
    13. Create an empty Customer when declaring the variable:

        ```
        customer: Customer = new Customer();
        ```


    14. Check the browser window for the new output.
    15. Check the browser console for the new output.


# 


# Model 5: Accept User Input in the App

Add an input field and submit button in the HTML and tie it to a getCustomerByNumber method.

We want to get a customer only after the user has specified a customer number, not when the app starts.


## Steps and Questions (20 min)



1. In the app.component.ts file, rename the ngOnInt() method, and add a parameter for the customer number:


```
getCustomerByNumber(number: number): void {
 this.http.get<Customer>('http://localhost:8080/customers/' + number)
   .subscribe(data => {
     this.customer = data;
   }
 );
}

```



2. In the app.component.html file, add an input field and submit button:

    ```
    <input #custnum type='text' placeholder="Customer number" 
        (keyup.enter)="getCustomerByNumber(custnum.value)"  
    />
    <button (click)="getCustomerByNumber(custnum.value)">
        Get customer
    </button>
    ```


3.  You should add at least one more customer to the database with Insomnia, so you can test your new code.
4. Switch to your browser window and try getting some customers.
5. Try getting a customer number that doesn’t exist in the database.
    1. What happened?
    2. Check the browser console. What is shown there?
6. We can add error handling to the GET:


```
this.http.get<Customer>('http://localhost:8080/customers/' + number)
 .subscribe(data => {
   this.customer = data;
 },
   err => {
     this.customer = new Customer();
     console.log('Error occurred.');
   }
);

```



7. Where does the error message display? Where would we like it to display?
8. First let’s get more information from the error:

    ```
    (error: HttpErrorResponse) => {
     this.customer = new Customer();
     console.log(error);
    ```


9. Test it again and check the console.
10. First, add a label to the HTML file:

    ```
    <label>{{errormessage}}</label>
    ```


11. Add an errormessage variable to the typescript file:

    ```
    errormessage: string = '';
    ```


12. Modify the error handler to set the errormessage:

    ```
    (error: HttpErrorResponse) => {
     this.customer = new Customer();
     this.errormessage = error.error;
    }
    ```


13. Go back to the browser.
    3. Do you see an error message before doing a GET?
    4. Do you see an error message when getting an existing customer?
    5. Do you see an error message when getting a non-existant customer?
    6. Do you see an error message when getting an existing customer again?
14. Clear the errormessage on a successful GET:

    ```
    .subscribe(data => {
     this.customer = data;
     this.errormessage = '';
    },

    ```



# 


# Model 6: Using Angular Directives to Modify the Document Structure

It would be nice to have add and remove elements from the HTML based on the behavior of the program. 

We can do that with Angular directives - in this case **<code>*ngIf</code></strong>. 

Lets only show the customer information if there is a customer object (this includes hiding the labels in front of number, firstName, and lastName. And let’s only show the error message label when there is an error object.


## Steps and Questions (10 min)



1. Let’s start off by defining customer and error variables, and setting them to null.
    1. We’ll do this in the class:

        ```
        export class AppComponent {
         title = 'activity13';
         customer: Customer = null;
         error: HttpErrorResponse = null;
        ```


    2. And we’ll reset them to null in the getCustomerByNumber() method:

        ```
        getCustomerByNumber(number: number): void {
         this.error = null;
         this.customer = null;
        ```


2. And we’ll only assign a value to one of them based on the results of the GET:


```
this.http.get<Customer>('http://localhost:8080/customers/' + number)
 .subscribe(data => {
   this.customer = data;
 },
   (error: HttpErrorResponse) => {
     this.error = error;
   }
);

```



3. Then we’ll only display the HTML elements if the correct object exists:
    3. We’ll do this for the label:

        ```
        <label *ngIf="error">{{error.error}}</label>
        ```


    4. And we’ll do this for the entire unordered list:

        ```
        <ul *ngIf="customer">
        ```


4. Go back to the browser and test it out.


# Model 7: Making REST API Calls - POST

Now let’s add a POST request to add a new customer.


## Steps and Questions (10 min)



1. Add to the HTML file to provide inputs for the first and last name, and a button to add the the new customer.


```
<h2>Add new customer.</h2>
<input #newcustfname type='text' placeholder="Customer first name"/>
<input #newcustlname type='text' placeholder="Customer last name"/>
<button (click)="addCustomer(newcustfname.value, newcustlname.value)">
  Add customer
</button>
<br>
<label *ngIf="custnumreturned">
  Customer created - customer number {{custnumreturned}}
</label>

```



2. (At this point you may want to remove the Angular logo, and add a second h2 header above the customer lookup form.)
3. Add to the typescript file:
    1. Add a variable for the returned customer number:

        ```
        custnumreturned: number = null;
        ```


    2. Add a method to add a new customer:

        ```
        addCustomer(firstName: string, lastName: string): void {
         this.http.post('http://localhost:8080/customers/new',
           {firstName: firstName, lastName: lastName})
           .subscribe(data => {
               this.custnumreturned = data;
             }
           );
        }
        ```


4. Go back to the browser and test it out.


# Model 8: **<code>*ngIf</code></strong> Again

Our interface is a bit confusing because we have two different forms with different purposes on the same page at the same time. Let’s change it so that we have some way to select between actions: Get customer or Add new customer.

We could have a single component and hide one or the other form with *ngIf.


## Steps and Questions (15 min)



1. First, let’s add radio buttons to select which form will be shown. Add this to your HTML file after the header elements:

    ```
    <form>
     <fieldset style="width:200px">
       <legend>Select an action</legend>
       <div>
         <input type="radio" id="get" name="app" [value]=1 checked 
    [(ngModel)]="appsection"/>
         <label for="get">Get existing customer</label>
       </div>
       <div>
         <input type="radio" id="add" name="app" [value]=2 
    [(ngModel)]="appsection"/>
         <label for="add">Add new customer</label>
       </div>
     </fieldset>
    </form>
    ```


2. This is using the **<code>[(ngModel)]</code></strong> which is sort of like a global variable space. It is part of the <strong><code>FormsModule</code></strong>.
    1. Add the <strong><code>FormsModule</code></strong> to the imports list in the <strong><code>app.module.ts</code></strong> file. You will also have to add an <strong><code>import</code></strong> statement for it.
3. Go back to the browser and see what this looks like. Try out the radio buttons. What happens?
4. This requires an <code>appsection</code> variable in the typescript file. Let’s start it at 1.

    ```
    appsection = 1;
    ```


5. In the HTML file, wrap each of the individual “forms” in <div></div> tags.
6. Add an *ngIf to each of those <div> tags, so that is is only shown when the corresponding radio button is clicked:
    2. For the customer lookup section:

        ```
        <div *ngIf="appsection==1">
        ```


    3. For the add new customer section:

        ```
        <div *ngIf="appsection==2">
        ```


7. Go back to the browser and try out the radio buttons.


# 


# Model 9: Angular Components

Now we can show or hide sections of the page, but it would be nice if we could group these parts of the page into reusable components.

The Angular component allows this. We have already seen one component - app.component - which is being included in our index.html with <app-root></app-root>.

We can make as many components as we wish, and even nest components inside other components.


## Steps and Questions (15 min)



1. First, let’s make an new customer component.
    1. Right-click on the app folder.
    2. Choose New > Angular Schematic…
    3. Choose component.
    4. Enter new-customer as the name.
2. Open the new-customer folder under the app folder.
    5. You will see that a component is composed of 4 files.
        1. The CSS file is used to specify visual style information about elements in the component.
        2. The HTML file is used to specify the display structure of the component.
        3. The SPEC.TS file is used to specify tests.
        4. The TS file is used to specify behavior and logic of the componenet.
3. Move the form information for adding a new customer from the app.component.html file to the new-customer.component.html file. Move just the part between the <div></div> tags.
4. Move the addNewCustomer() method from the app.component.ts file to the new-customer.component.ts file.
    6. You will also need to add to new-customer.component.ts:
        5. The HttpClient import
        6. The HttpClient parameter to the constructor
        7. The custnum variable
5. Replace the code you removed between the <div></div> tags with the tags for the new-customer component:

        ```
        <app-new-customer></app-new-customer>
        ```


6. Go back to the browser to test it.
7. Do the same thing with the get customer form.

    

<p id="gdcalert2" ><span style="color: red; font-weight: bold">>>>>>  gd2md-html alert: inline image link here (to images/image1.png). Store image on your image server and adjust path/filename/extension if necessary. </span><br>(<a href="#">Back to top</a>)(<a href="#gdcalert3">Next alert</a>)<br><span style="color: red; font-weight: bold">>>>>> </span></p>


![alt_text](images/image1.png "image_tooltip")
Copyright © 2019 Karl R. Wurst. This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
