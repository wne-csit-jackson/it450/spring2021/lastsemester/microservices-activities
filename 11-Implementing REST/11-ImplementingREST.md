# Implementing New REST API Calls

## Content Learning Objectives


_After completing this activity, students should be able to:_

*

## Process Skill Goals

_During the activity, students should make progress toward:_

*

## Team Roles

_Decide what role each of you will play for today. Choose a role that you have not played before, or recently. The goal should be to have all team members rotate through the roles on a regular basis to become comfortable with all the roles. If you have only three people, one should have two roles. If you have five people, two may share the same role. Record role assignments here._


<table>
  <tr>
   <td>
Manager
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>Presenter
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>Recorder
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>Technician/
<br>
Reflector
   </td>
   <td>
   </td>
  </tr>
</table>

# Model 4: Implementing New Endpoints


## Questions (Remaining Class Time)



15. Implement your REST API request to get an array containing an array of customer objects, that includes all customers in the database.
16. Implement your REST API request to change the first name and lastname of a customer, when you know the customer number.
17. If you have still have time, go on to Model 5


# 


# Model 5: Optional - Even More Endpoints


## Questions



18. Design and implement a REST API request to get a customer’s number given their first and last names.
19. Design and implement a REST API request to get all of a customer’s order numbers given their customer number.
20. Design and implement a REST API request to generate a full “order form” given an order number. In other words, a single JSON object with the customer number, first name, last name, all the order lines with SKU, description, unit price, quantity, and total price per line, as well as total price for the entire order.
---

&copy; 2020 Karl R. Wurst <karl@w-sts.com>

<img src="http://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/]() or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.