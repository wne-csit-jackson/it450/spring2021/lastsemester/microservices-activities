# Introduction to REST API Calls

## Content Learning Objectives


_After completing this activity, students should be able to:_

*

## Process Skill Goals

_During the activity, students should make progress toward:_

*

## Team Roles

_Decide what role each of you will play for today. Choose a role that you have not played before, or recently. The goal should be to have all team members rotate through the roles on a regular basis to become comfortable with all the roles. If you have only three people, one should have two roles. If you have five people, two may share the same role. Record role assignments here._


<table>
  <tr>
   <td>
Manager
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>Presenter
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>Recorder
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>Technician/
<br>
Reflector
   </td>
   <td>
   </td>
  </tr>
</table>

## Model 1: OrderModule Data Schema

This model represents the data in the OrderModule. It stores information about individual orders and their status.

### Items Collection
_id | name
--- | ---
5f4173ef3067093823a77cf8 | Canned fruit
5f4173ef3067093823a77cf7 | Canned vegetables
5f4173ef3067093823a77cf6 | Soup
5f4173ef3067093823a77cf5 | Pasta sauce
5f4173ef3067093823a77cf4 | Pasta
5f4173ef3067093823a77cf3 | Cereal
5f4173ef3067093823a77cf2 | Tuna
5f4173ef3067093823a77cf1 | Peanut butter

### Orders Collection
_id | preferences | restrictions | items | email
--- | --- | --- | --- | ---
5f41732722b68475ed8e762e | Pineapple, Froot Loops | | Canned fruit,<br>Cereal | jandrese@college.edu
5f41732722b68475ed8e762d | | Peanut allergy | Pasta,<br>Pasta Sauce | tbeck@college.edu
5f41732722b68475ed8e762c | | | Peanut butter,<br>Soup,<br>Cereal | mschwartz@college.edu

### Orders Approvals Collection
_id | preferences | restrictions | items | email | reviewStatus
--- | --- | --- | --- | --- | ---
5f41732722b68475ed8e762d | | Peanut allergy | Pasta,<br>Pasta Sauce | tbeck@college.edu | approved
5f41732722b68475ed8e762c | | | Peanut butter,<br>Soup,<br>Cereal | mschwartz@college.edu | unreviewed

### Questions (10 min)

1. What is order id for the person with the email tbeck@college.edu?
2. What food item has the id of 5f4173ef3067093823a77cf3?
3. What type of cereal would jandrese@college.edu prefer to receive?
4. How many food items has mschwartz@college.edu requested?
5. Which order has not yet been reviewed?
6. Which order is not yet in the Orders Approvals Collection? Why do you think that might that be? 
7. What do you think is the process an order goes through from being placed to being approved, in terms of the data?

## Model 2: REST APIs

Web service APIs that adhere to the REST architectural constraints are called RESTful APIs. HTTP-based RESTful APIs are defined with the following aspects:

- a base URL, such as http://api.example.com/resources
- a media type for any data to be sent to the server
- standard HTTP methods (e.g., GET, PUT, PATCH, POST, and DELETE).

### Relationship between URL and HTTP methods

The following table shows how HTTP methods are typically used in a RESTful API:

<table>
  <tr>
   <td>
Uniform Resource Locator (URL)
   </td>
   <td>GET
   </td>
   <td>PUT
   </td>
   <td>PATCH
   </td>
   <td>POST
   </td>
   <td>DELETE
   </td>
  </tr>
  <tr>
   <td>Collection, such as https://api.example.com/resources/
   </td>
   <td><strong>List</strong> the URIs and perhaps other details of the collection's members. 
   </td>
   <td><strong>Replace</strong> the entire collection with another collection. 
   </td>
   <td>Not generally used 
   </td>
   <td><strong>Create</strong> a new entry in the collection. The new entry's URI is assigned automatically and is usually returned by the operation.
   </td>
   <td><strong>Delete</strong> the entire collection. 
   </td>
  </tr>
  <tr>
   <td>Element, such as https://api.example.com/resources/item17
   </td>
   <td><strong>Retrieve</strong> a representation of the addressed member of the collection, expressed in an appropriate Internet media type.
   </td>
   <td><strong>Replace</strong> the addressed member of the collection, or if it does not exist, <strong>create</strong> it. 
   </td>
   <td><strong>Update</strong> the addressed member of the collection. 
   </td>
   <td>Not generally used. Treat the addressed member as a collection in its own right and <strong>create</strong> a new entry within it
   </td>
   <td><strong>Delete</strong> the addressed member of the collection. 
   </td>
  </tr>
</table>

From [https://en.wikipedia.org/w/index.php?title=Representational_state_transfer&oldid=866461276](https://en.wikipedia.org/w/index.php?title=Representational_state_transfer&oldid=866461276) retrieved 30 October 2018.

## Model 3: Exploring the Order Module API

### Get and Start the Microservices Example Project

1. Clone the [Microservices Example project](https://gitlab.com/LibreFoodPantry/training/microservices-example) from LibreFoodPantry.
2. Open the README.md file
   1. Follow the steps under `Setup`. This will involve installing the following (if you do not already have them installed):
      - Bash
      - Git
      - Docker Desktop
   Note that there are install instructions for both MacOS and Windows OS computers.
3. Open a terminal/command window inside the top-level directory of the cloned project.
4. Build and run the development shell. (This will need to be down before executing any of the `make` commands):

   ```bash
   shell/run.sh
   ```

3. Start the system with the command:

   ```bash
   make build up
   ```

### OrderModule REST API

- To view the API documentation for the Place Order feature, open the URL [http://localhost:10001/api-docs/](http://localhost:10001/api-docs/)

   - You will see both the Place Order and Approve Order features. Collapse the Approve Order feature.
   - You should also collapse the Schemas section.

- To view the API documentation for the Approve Order feature, open the URL [http://localhost:10011/api-docs/](http://localhost:10011/api-docs/)

   - You will see both the Place Order and Approve Order features. Collapse the Place Order feature.
   - You should also collapse the Schemas section.

### Questions (? min)

1. How many REST API endpoints are there for: (List them)

   1. Place Order
   2. Approve Order

2. How many HTTP methods are used? List them.
3. Based on the notes in table above, what is each of the HTTP methods used for in

   1. Place Order
   2. Approve Order
4. Look at the Place Order endpoints.
   1. Expand the `GET /items` endpoint by clicking on it.
      1. Look at the Parameters section.
         1. List and explain the parameters
         2. What do the `+` and `-` in front of `created` represent?
      2. Look at the Server Response section.
         1. You may want to look at the [list of HTTP response codes](https://en.wikipedia.org/wiki/List_of_HTTP_status_codes).
         2. What is the format of the response body? Explain what it contains and how it is formatted.
         3. Compare the response body to what you saw in Model 1.
   2. Do the same for `GET /orders`.
      1. What is different?
      2. Explain the difference.
   3. Do the same for `POST /orders`.
      1. What new section appears that was not in either of the `GET` requests?
      2. Explain the value in this section.
      3. What is different in the response section?
      4. Explain why that is different than in the `GET` requests.
   4. Do the same for `GET /orders/{id}`.
      1. What is different about the parameters?
      2. What is different about the responses? Both numbers and format.
5. Look at all the Approve Order endpoints. Look for, describe, and explain similarities and differences compared to Place Order.
   1. In particular, look the two `PATCH` endpoints.

## Stop the Microservices Example Project

When finished, stop the system with the command:

   ```bash
   make down
   ```

---

&copy; 2020 Karl R. Wurst <karl@w-sts.com>

<img src="http://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/]() or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
