# Exploring REST API Implementation

## Content Learning Objectives


_After completing this activity, students should be able to:_

*

## Process Skill Goals

_During the activity, students should make progress toward:_

*

## Team Roles

_Decide what role each of you will play for today. Choose a role that you have not played before, or recently. The goal should be to have all team members rotate through the roles on a regular basis to become comfortable with all the roles. If you have only three people, one should have two roles. If you have five people, two may share the same role. Record role assignments here._


<table>
  <tr>
   <td>
Manager
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>Presenter
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>Recorder
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>Technician/
<br>
Reflector
   </td>
   <td>
   </td>
  </tr>
</table>

# Model 1: Directory Structure

Have your technician share their screen and open the [Microservices Example project](https://gitlab.com/LibreFoodPantry/training/microservices-example) in an IDE.

Each of the two features, Place Order and Approve Order, is implemented by a *service*, in this case written in [Node.js](https://nodejs.org) using the [Express.js](https://expressjs.com) framework. The Express.js framework provides server functionality for this example.

Most of the code for this example is generated from the `openapi.yml` file by the [openapi-generator](https://openapi-generator.tech) tool. It produces code for the server, using Express.js, and skeleton code for the two services.

The directory structure and build process for this example is just one of many possible. When encountering other tools their structure and build process may be different. For full details of this project's directory structure and build process, please read [this document](https://gitlab.com/LibreFoodPantry/training/microservices-example/-/blob/master/BuildProcess.md). You do not need the full details for this activity, but you will for any associated homeworks.

### Questions

1. Run `make build` before answering Question 2.
2. In the `OrderModule` directory, look at both the `approve-order-api` subdirectory and the `place-order-api` subdirectory and compare their contents. You should find one difference between the two subdirectories (ignore the `place-order-db-seeder` subdirectory - we will explore that later.)
    1. What is the difference you found between the two?
    2. For each of the two services, compare the contents of the file you discovered with the file of the same name in the `build` subdirectory. What is the difference between the two files with the same name in each of the services?
    2. Based on what was described above about the code being generated, why do you think this difference is here?

# Model 2: Services

Open the `OrderModule/place-order-api/src/services/PlaceOrder.js` file.

## Questions

1. Starting at the bottom of the file, look at the exports. Where have you seen these names before? Where do they come from?
2. Find the `listItems` function.
    1. A `promise` is an object that represents the eventual completion of an asynchronous operation. Look at the code. Why does this need to be asynchronous?
    2. What are `resolve` and `reject`? How are they being used here?
    3. What is the `getPlaceOrderDatabaseCollection` helper function doing?
    4. What is the `getPageOfDocuments` helper function doing? You don't need to get into all the details at this point, just get a general idea of what it does based on function name and parameters.
    5. Why do you think we need `getPageOfDocuments`?
3. Compare `getOrders` to `listItems`. What is the difference between the two?
4. Compare `getOrder` to `getOrders`.
    1. What differences do you see in the parameters? Why are there those differences?
    2. What differences do you see in the two lines that access the database? Why?
    3. What differences do you see in the responses? List each response and explain what causes that response.
5. Look at the `placeOrder` function.
    1. What is the parameter, what does it represent, and where does it come from?
    2. What is the first block of code in the `try` (lines 128-139) doing and why? This [link](https://docs.mongodb.com/manual/reference/method/db.collection.find/) may help with line 129.
    3. What is the second block of code in the `try` (lines 141-147) doing and why?
    4. What is the third block of code in the `try` (lines 149-155) doing and why?
    5. The `resolve` returns the `id` of the new order (this is probably not the clearest way to do this.)
    6. What are the two possible results of the `reject`? What is each one for?

Open the `OrderModule/approve-order-api/src/services/ApproveOrder.js` file.

6. Starting at the bottom of the file, look at the exports. Where have you seen these names before? Where do they come from?
7. Look at the `approveOrder` and `cancelOrder` functions.
    1. What is `foundOrderAndModifiedStatus` doing? 
        1. What are its parameters?
        2. What does it return?
    2. What is the purpose of the `if (success) ...` block?
8. Look at `getApprovedOrders`. How does it differ from `getOrders` in `place-order-api`?
9. Compare `getUnreviewedOrders` to `getApprovedOrders`.
    1. How do lines 145-155 of `getUnreviewedOrders` differ from lines 99-108 of `getApprovedOrders`?
    2. What is the first code block (lines 121-124) doing?
    3. Lines 123-143 check to see if any new orders have been placed that are not yet in the Approve Order database, and copies them into the Approve Order database. When will this be called? (Explain in terms of the workflow and staff at the food pantry.)
        1. What is the purpose of the URI in line 126?
        2. What is the purpose of the token in line 127?
        3. What is the result of lines 133-134?
        4. What is the maximum number of elements in that list? How do you know?
        5. What is the purpose of lines 135-138?
        6. What is the purpose of lines 140-142?
        7. What is the purpose of the `do-while` loop from line 131 to line 143?
        8. When will the loop end? How is that determined?

---

&copy; 2020 Karl R. Wurst <karl@w-sts.com>

<img src="http://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/]() or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.